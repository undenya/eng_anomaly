Fixed English Localization v0.1 for Anomaly 1.5.1 + some addons ︻╦╤─
Виправлена Англійська Локалізація v0.1 для Anomaly 1.5.1 та декількох аддонів

The addon replaces Russian shit like "Putinka" or "RUB" with normal Ukrainian counterparts. For example, "Banderivka" and "UAH".
And renew some textures. For example, add modern Military Faction Patch and some translated textures.

Chornobyl is located on the territory of Ukraine. 
Ukraine is not Russia. And this is not politics! 
Ukrainians and Russians are two different peoples with completely different cultures.
Ukrainian goods and Ukrainian currency is easier enter the Zone. This is logical.
The fact that stalkers can make faction flags and patches, but 30 years cannot update old Soviet posters in Bar - is nonsense.

Now 3 folders in the archive:
-1- Text - Main - fixed text of the main game
-2- Textures - Main - new textures of the main game
-3- Text - Boomsticks and Sharpsticks - fixed text of BaS addon 23-Apr-2021 (updated April 23, 2021)

Addon repository
https://bitbucket.org/undenya/eng_anomaly

And this is a link to support the developer.
https://new.donatepay.ru/en/@undenya

Support the developer! ☝️

PS: sorry for my english 😃

…...........(\(\....... ....../)/)
…...........(=':')..........(':'=)
……........(.(")(").......(")(").)
…..…._•*`°``*•-., .-•*`°``*•-.,_

Аддон замінює російське лайно, таке як "Путінка" або "рублі", на українські аналоги. Наприклад, "Бандерівка" та "гривні".
Та оновює деякі текстури. Наприклад, додає сучасний патч Військових та деякі перекладені текстури.

Чорнобиль знаходиться на території України.
Україна - це не Росія. І це не політика!
Українці та росіяни - це два різні народи із абсолютно різними культурами.
Українським товарам та українській валюті легше потрапити в Зону. Це логічно.
Той факт, що сталкери можуть зробити прапори фракцій та нашивки, але 30 років не можуть оновити старі совєтські плакати в Барі - безглуздя.

Тепер 3 папки в архіві:
-1- Текст - Основний - виправлений текст основної гри
-2- Текстури - Основні - нові текстури основної гри
-3- Text - Boomsticks and Sharpsticks - виправлений текст BaS аддону 23 квітня 2021 року (оновлено 23 квітня 2021 року)

Репозиторій аддону за посиланням
https://bitbucket.org/undenya/eng_anomaly

А це посилання для тих, хто хоче підтримати просування українського, гривнею.
https://uncle-denya.diaka.ua/ua-anomaly 

Підтримуй українське! ☝️

А ще не забувайте про вподобайку, підписку та дзвіночок... Чи що тут є з цього? 😃
